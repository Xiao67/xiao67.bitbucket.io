let card = document.querySelector(".card");
[...card.children].reverse().forEach(i => card.append(i));

card.addEventListener("click",change);

function change(event){
    let service = document.querySelector(".service:last-child");
    if (event.target !== service) return;
    service.style.animation = "swap 400ms forwards";

    setTimeout(() => {
        service.style.animation = "";
        card.prepend(service);
        
    },400);
}